
# Table of Contents

1.  [Project Information](#org744eb32)
2.  [Event Information](#orgdc0ea0b)
3.  [Websites for Inspiration](#org83215d0)



<a id="org744eb32"></a>

# Project Information

This website was created in 2019 by Eric S. Londres for Alex Wellerstein. All rights reserved.

This project is written in React, using [Create React App](<https://facebook.github.io/create-react-app/>).


<a id="orgdc0ea0b"></a>

# Event Information

You are invited to join us for the second Reinventing Civil Defense workshop on August 9-10, 2019 at Stevens Institute of Technology in Hoboken, NJ. 

The workshop theme is “Building the Future." Friday, August 9th will include a workshop with panel discussions and a showcase of several newly developed nuclear risk communication tools. Then, Saturday, August 10 will continue the showcase with an emphasis on targeting a youth audience for feedback. More information on the agenda is forthcoming.

Fill out the following RSVP form by May 31 to reserve your spot. ****Space is limited.**** 

<https://forms.gle/M68cL97Kcn6C6iwn8>

We will reach out in June with more information for those that are able to attend. Nearby accommodations will be available at a discounted rate (details forthcoming). 

Reinventing Civil Defense is a project at the Stevens Institute of Technology, funded by a grant from the Carnegie Corporation of New York. The Reinventing Civil Defense project seeks to create frameworks for new kinds of public engagement about nuclear risk. More specifically, it seeks to convene a diverse group of stakeholders and experts to foster dialogue, create focal points for discussion, develop new media tools, and create concrete communications strategies that attract a new generation of individuals to engage directly and meaningfully with nuclear issues. 

More information can be found at: <https://reinventingcivildefense.org/>

If you have any questions, you can reach out to the RCD team atreinventingcivildefense@stevens.edu.

We hope to see you in person in August.


<a id="org83215d0"></a>

# Websites for Inspiration

<http://bombshelltoe.com/>

<https://www.ploughshares.org/issues-analysis/article/chain-reaction-2019-new-moment>

