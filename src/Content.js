import React from 'react';
import { Parallax } from 'react-scroll-parallax';
import './Content.css';

function Content () {
    return (
        <Parallax> {/* The parallax wrapper is needed so the div renders over the header, even though no parallaxing is actually applied. */}
          <div className="Content">
            <p>Reinventing Civil Defense is holding its second workshop, <strong>Building the Future</strong>, on Friday, August 9th and Saturday, August 10th at Stevens Institute of Technology. Friday's workshop will include panel discussions and a showcase of several newly developed nuclear risk communication tools. Then, Saturday will continue the showcase with an emphasis on targeting a youth audience for feedback.</p>
            
            <p>More information on the agenda and nearby accommodations is forthcoming.</p>

            <hr></hr>
            
            <p>Reinventing Civil Defense is a project at the Stevens Institute of Technology, funded by a grant from the Carnegie Corporation of New York. The Reinventing Civil Defense project seeks to create frameworks for new kinds of public engagement about nuclear risk. More specifically, it seeks to convene a diverse group of stakeholders and experts to foster dialogue, create focal points for discussion, develop new media tools, and create concrete communications strategies that attract a new generation of individuals to engage directly and meaningfully with nuclear issues.</p>

            <p>More information on the project can be found <a href="https://reinventingcivildefense.org/">on our website</a>. If you have any questions, you can reach out to the team at <a href="mailto:reinventingcivildefense@stevens.edu">ReinventingCivilDefense@stevens.edu</a>.</p>
            
          </div>
        </Parallax>
    );
}

export default Content;

