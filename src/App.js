import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ParallaxProvider } from 'react-scroll-parallax';
import Header from './Header';
import Content from './Content';
import './App.css';

function App() {
    return (
        <div className="App">
          <CssBaseline/>
          <ParallaxProvider>
            <Header/>
            <Content/>
          </ParallaxProvider>
        </div>
    );
}

export default App;
