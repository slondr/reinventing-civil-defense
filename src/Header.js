import React from 'react';
import { Parallax } from 'react-scroll-parallax';
import logo from './logo.svg';
import './Header.css';

function Header() {
    return (
        <header className="App-header">
          <Parallax y={[-10, 30]}>
            <img src={logo} className="App-logo" alt="logo" />
            <h3>Reinventing Civil Defense</h3>
            <h1>Building the Future</h1>
            <p>August 9th & 10th</p>
            <p>Stevens Institute of Technology</p>
          </Parallax>
        </header>
    );
}

export default Header;
